#!/usr/bin/env python3

from snipsTools import SnipsConfigParser
from hermes_python.hermes import Hermes
import datetime
from dateutil.parser import parse
import requests

CONFIG_INI = "config.ini"

MQTT_IP_ADDR = "localhost"
MQTT_PORT = 1883
MQTT_ADDR = "{}:{}".format(MQTT_IP_ADDR, str(MQTT_PORT))
USERNAME_INTENTS = "jsemig"
SHIP_DATA_URL = 'https://tuic-ms3-api.prelive.cellular.de/cruise/data/time'


class TuiTimeAndDate(object):
    """Class used to wrap action code with mqtt connection

        Please change the name refering to your application
    """

    def __init__(self):
        # get the configuration if needed
        try:
            self.config = SnipsConfigParser.read_configuration_file(CONFIG_INI)
        except:
            self.config = None

        # start listening to MQTT
        self.start_blocking()

    @staticmethod
    def user_intent(intentname):
        return USERNAME_INTENTS + ":" + intentname


    def temperatur_intent(self, hermes, intent_message):
        result_sentence = None
        if intent_message.slots.Temperatur:
            print(intent_message.slots.Temperatur.first().value)
            result_sentence = "Ok, ich stelle die Temperatur auf {0} Grad.".\
                format(int(intent_message.slots.Temperatur.first().value))
        elif intent_message.slots.hoch_runter:
            print(intent_message.slots.hoch_runter.first().value)
            if intent_message.slots.hoch_runter.first().value == 'hoch':
                result_sentence = "Ok, ich stelle die Temperatur auf 25 Grad."
            else:
                result_sentence = "Ok, ich stelle die Temperatur auf 21 Grad."

        if result_sentence is None:
            result_sentence = "Ok, ich stelle die Temperatur auf 23 Grad."
        current_session_id = intent_message.session_id
        hermes.publish_end_session(current_session_id, result_sentence)


    def light_intent(self, hermes, intent_message):
        print(intent_message.slots.on_off.first().value)
         # return response
        result_sentence = "ok"
        current_session_id = intent_message.session_id
        hermes.publish_end_session(current_session_id, result_sentence)

    def vorhaenge_intent(self, hermes, intent_message):
        print(intent_message.slots.auf_zu.first().value)
         # return response
        result_sentence = "ok"
        current_session_id = intent_message.session_id
        hermes.publish_end_session(current_session_id, result_sentence)

    # --> Master callback function, triggered everytime an intent is recognized
    def master_intent_callback(self, hermes, intent_message):
        coming_intent = intent_message.intent.intent_name
        if coming_intent == self.user_intent('Temperatur'):
            self.temperatur_intent(hermes, intent_message)
        elif coming_intent == self.user_intent('Licht'):
            self.light_intent(hermes, intent_message)
        elif coming_intent == self.user_intent('Vorhaenge'):
            self.vorhaenge_intent(hermes, intent_message)

    # --> Register callback function and start MQTT
    def start_blocking(self):
        with Hermes(MQTT_ADDR) as h:
            h.subscribe_intents(self.master_intent_callback).start()


if __name__ == "__main__":
    TuiTimeAndDate()
